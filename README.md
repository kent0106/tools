# 工具集


1. goimports  mac版  修改import的顺序为 标准版 公司内部库 第三方库
2. genschema  mac版  拉取表结构到本地 项目目录下配置genschema.json文件后运行
```json
{
  "db":{
    "name":"name",
    "host":"host",
    "user":"user",
    "password":"password",
    "port":13306
  },
  "packages":[
    {
      "path":"internal/sql/schema",
      "table_names":[
        "atomic_index",
        "time_period_item"
      ]
    }
  ]
}
```
